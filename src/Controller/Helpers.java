
package Controller;

import java.util.Scanner;


public class Helpers {
    Scanner in;

    public Helpers() {
        in = new Scanner(System.in);
    }
    
    //check int
    //check number
    
    public int checkInt(){
        String n ="";
        while(true){
            try {
                n = in.nextLine();
                return Integer.parseInt(n);
            } catch(Exception e){
                System.out.println("Enter again");
            }
        }
    }
    
    public boolean checkAcc(String account){
        String regex = "^[0-9]{10}$";
       return  account.matches(regex);
    }
    
    public boolean checkPass(String pass){
        String regex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,31}$";
        return pass.matches(regex);
    }
    public String genCaptcha(){
        //captcha gom 6 ki tu
        String chars = "qwertyuiopasdfghjklzxcvbnm0123456789QWERTYUIOPASDFGHJKLZXCVBNM!@#$";
        String captcha="";
        while(captcha.length() <6){
            int random = (int) (Math.random()* chars.length()-1);
            captcha += chars.charAt(random);
        }
        return captcha;
    }
    
}
