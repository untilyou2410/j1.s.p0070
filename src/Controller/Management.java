package Controller;

import Model.Account;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Management {

    public Management() {
    }

    public void menuString() {
        System.out.println("-------Login Program-------\n"
                + "1. Vietnamese\n"
                + "2. English\n"
                + "3. Exit\n"
                + "Please choice one option:");
    }

    public void menu() {
        Scanner in = new Scanner(System.in);
        Helpers helpers = new Helpers();
        int option;
        
        while(true){
            menuString();
            option = helpers.checkInt();
            switch (option){
                case 1: Locale localeVn = new Locale("vn","VN");
                        login(localeVn);
                        break;
                case 2 : Locale localeEn = new Locale("en","EN");
                        login(localeEn);
                        break;
                case 3: return;
                
            }
        }
        
    }

    
    public void login(Locale locale ){
        Helpers helpers = new Helpers();
        Scanner in = new Scanner(System.in);
        
         ResourceBundle rsb = ResourceBundle.getBundle("Language/MessagesBundle", locale);
         String acc="";
         while(true){
             System.out.println(rsb.getString("enterAcc"));
             acc= in.nextLine();
             if(helpers.checkAcc(acc)){
                 break;
             }
             System.out.println(rsb.getString("checkAcc"));
         }
         
          String pass="";
         while(true){
             System.out.println(rsb.getString("enterPass"));
             pass= in.nextLine();
             if(helpers.checkPass(pass)){
                 break;
             }
             System.out.println(rsb.getString("checkPass"));
         }
         
         String cap = helpers.genCaptcha();
         String capIn="";
         while(true){
             System.out.println("Capchar  "+cap );
             System.out.println(rsb.getString("enterCaptcha"));
             capIn= in.nextLine();
             if(capIn.contentEquals(cap)){
                 break;
             }
             System.out.println(rsb.getString("checkCaptcha"));
         }
     
    }
}
